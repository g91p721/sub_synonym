"""Add whitespace noise to a string.

This module contains a function that adds whitespace "noise" to a string. This
module also serves as documentation on how to write and document a Python
function in the context of a scientific computing collaboration. This module is
intended to stand alone. *It is not part of a package.*

Third-party packages required (``requirements.txt`` format)::

    tiktoken==0.3.3

This module has been checked using the following tools:

* ``black --line-length=119``
* ``ruff``
* ``pycodestyle pycodestyle --ignore=E501 whitespacenoise.py``

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""
# Tip: additional Google Python Style Guide docstring examples available here:
# https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html


import tiktoken
import random

enc = tiktoken.get_encoding("gpt2")
whitespace_encoded = enc.encode(" ")[0]


def add_whitespace_noise(s: str, random_seed: int = None) -> str:
    """Add whitespace at a random position (respecting word boundaries).

    Args:
        s: The string to which noise will be added.
        random_seed: Random seed.

    Returns:
        String with whitespace noise added.

    """
    random.seed(random_seed)
    tokenized = enc.encode(s)
    tokenized.insert(random.randrange(len(tokenized) + 1), whitespace_encoded)
    return enc.decode(tokenized)


# additional "documentation" in the form of simple tests
if __name__ == "__main__":
    # sentence is from nyt_eng
    sentence = (
        """Jolene Coyle, the owner of Job Place, placed a banner Saturday bearing a statement """
        """against same-sex marriages outside a Granard Street business building she owns."""
    )
    expected = (
        """Jolene Coyle, the owner  of Job Place, placed a banner Saturday bearing a statement """
        """against same-sex marriages outside a Granard Street business building she owns."""
    )
    assert add_whitespace_noise(sentence, random_seed=1) == expected
    assert add_whitespace_noise("building", random_seed=1) == " building"
    assert add_whitespace_noise("", random_seed=1) == " "
