"""Substitute a non-stop word in a string.

This module contains a function that substitutes a non-stop word to its synonym in a
string. This module has two data files. One is the official GloVe-6B_ (trained on
Wikipedia 2014 & Gigaword 5) embeddings file; The other is a function word list of 512
words tokenized with a Moses tokenizer.

Third-party packages required (``requirements.txt`` format)::

    faiss-cpu==1.7.4
    sacremoses==0.0.53
    numpy==1.24.3

This module has been checked using the following tools:

* ``black subsysnonym.py  --line-length=88``
* ``ruff subsysnonym.py``
* ``pycodestyle --ignore=E501 subsysnonym.py``

Examples:
    >>> s = 'The quick brown fox jumps over the lazy dog.'
    >>> sub_synonym(s)
    'The quick brown fox jumps over the lazy puppy.'

.. _GloVe-6B:
   https://nlp.stanford.edu/projects/glove/

"""

import os
import faiss
import random
import zipfile
import numpy as np
from urllib import request
from string import punctuation
from sacremoses import MosesTokenizer, MosesDetokenizer


glv6b = "glove.6B.300d.txt"
glv6b_ulr = "https://downloads.cs.stanford.edu/nlp/data/glove.6B.zip"
koppel512_url = (
    "https://raw.githubusercontent.com/psal/anonymouth/master/src/edu/drexel/psal/"
    "resources/koppel_function_words.txt"
)

# get embedding if not available locally
if not os.path.isfile(glv6b):
    print("GloVe-6B not found, try to retrieve it. This may take several minutes.")
    glv6b_zip, _ = request.urlretrieve(glv6b_ulr)
    with zipfile.ZipFile(glv6b_zip) as f:
        f.extract(glv6b)
    del glv6b_zip
    print("Done downloading.")

# get stop words list
if not os.path.isfile(koppel512_url.split("/")[-1]):
    print("Stop words list not found, try to retrieve it.")
    request.urlretrieve(koppel512_url, koppel512_url.split("/")[-1])
    print("Done downloading.")
stop_words = open(koppel512_url.split("/")[-1]).read().splitlines()

# init (de)tokenizer, vocab, and faiss-indexed embeddings
tokenizer = MosesTokenizer(lang="en")
detokenizer = MosesDetokenizer(lang="en")
vocab = []
embeddings = np.empty((400000, 300))
with open(glv6b) as f:
    idx = 0
    for line in f:
        values = line.split()
        vocab.append(values[0])
        embeddings[idx] = np.asarray(values[1:])
        idx += 1

print("Indexing embeddings with faiss, it may take several minutes.")
f_index = faiss.IndexFlatL2(300)
f_index.add(embeddings)
print("Done indexing.")


def sub_synonym(sentence: str, random_state: int = None) -> str:
    """Replace a word in an English string with one of its 10 nearest neighbors found in
    GloVe-B embeddings. Stop words, punctuation, and digits will not be replaced. In
    rare cases where no suitable word is found based on the aforementioned criteria, the
    original sentence will be returned.

    Args:
        sentence: An English string whose
        random_state: A Random seed.

    Returns:
        A string with one word changed to its synonym or its original form.

    """
    random.seed(random_state)
    # find a non-stop word token to replace
    tokens = tokenizer.tokenize(sentence)
    tokens_tmp = [
        t
        for t in tokens
        if not ((t.lower() in stop_words + list(punctuation)) or t.isdigit())
    ]
    # in case there is no candidate token to replace
    if len(tokens_tmp):
        token_to_sub = random.choice(tokens_tmp)
    else:
        return sentence
    # in case the chosen token is not in glove vocab
    try:
        q = embeddings[vocab.index(token_to_sub.lower())].reshape(1, -1)
    except ValueError:
        # original string will be returned if token to substitute not in vocab
        return sentence
    # sample from 10 nearest neighbors
    _, syn_indices = f_index.search(q, 10 + 1)
    syn = vocab[random.choice(syn_indices.ravel()[1:])]
    position = [t.lower() for t in tokens].index(token_to_sub.lower())
    # preserve capitalization naively
    tokens[position] = syn if token_to_sub[0].islower() else syn.capitalize()

    return detokenizer.detokenize(tokens)


# additional "documentation" in the form of simple tests
if __name__ == "__main__":
    # sentence is from nyt_eng
    sentence = (
        """Jolene Coyle, the owner of Job Place, placed a banner Saturday bearing """
        """a statement against same-sex marriages outside a Granard Street business """
        """building she owns."""
    )
    expected = (
        """Jolene Coyle, the owner of Job Place, placed a banner Saturday bearing """
        """a statement against same-sex marriages outside a Granard Street business """
        """building she purchased."""
    )
    assert sub_synonym(sentence, random_state=17) == expected
    assert sub_synonym("This is 123.") == "This is 123."
    assert sub_synonym("") == ""
