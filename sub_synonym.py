"""Substitute a non-stop word in a string.

This module contains a function that replaces a non-stop word with its synonym in an
English string. The workflow is as follows: 1) The document is tokenized using Moses;
2) A token is randomly selected for substitution, excluding stop words, digits,
and punctuation; 3) One of the ten nearest neighbors of the chosen token in the
[GloVe-6B (300D)]_ space is randomly selected; 4) The function outputs detokenized
tokens, where the first occurrence of the chosen token is replaced with its neighbor.

Third-party packages required (``requirements.txt`` format)::

    faiss-cpu==1.7.4
    sacremoses==0.0.53
    numpy==1.24.3

Before use, the embeddings should be downloaded and unzipped. The path to .txt file
hosting GloVe-6B should be ported to the environment variable `GLOVE6B300D_PATH`::

$ curl -o /path/to/glove.6B.zip https://downloads.cs.stanford.edu/nlp/data/glove.6B.zip
$ unzip -o /path/to/glove.6B.zip glove.6B.300d.txt
$ export GLOVE6B300D_PATH=/path/to/glove.6B.300d.txt

This module has been checked using the following tools:

* ``black sub_synonym.py``
* ``ruff sub_synonym.py``
* ``pycodestyle --ignore=E501 sub_synonym.py``

Examples:

>>> s = 'The quick brown fox jumps over the lazy dog.'
>>> sub_synonym(s, 2)
'The fast brown fox jumps over the lazy dog.'

.. [GloVe-6B (300D)]
   https://nlp.stanford.edu/projects/glove/

"""

import os
import faiss
import random
import numpy as np
from typing import Optional
from string import punctuation
from sacremoses import MosesTokenizer, MosesDetokenizer

# get path to glove-6b from the environment variable
try:
    glv6b = os.environ["GLOVE6B300D_PATH"]
    print(f"Path to GloVe-6B is {glv6b}.")
except KeyError:
    raise SystemExit(
        "`GLOVE6B300D_PATH` env var not found. Check docstring for " "details."
    )

# a list of 512 stop words (to ignore in substitution)
# https://raw.githubusercontent.com/psal/anonymouth/master/src/edu/drexel/psal/
# resources/koppel_function_words.txt
# sha256: 49bcaab11d737d7f6f29e4df92798ea385f4e32d2e984edc48ac9c8f7098bf18
# fmt: off
STOP_WORDS = {'anybody', 'sixthly', "he's", "e'g'", 'doubtful', 'v', 'moreover',
              'ninth', 'are', 'whither', 'thrice', 'thousandth', "it'll", 'presumable',
              'unusual', 'still', 'millionth', 'w', 'it', "didn't", 'indeed', 'ay',
              "shouldn't", 'again', 'out', 'during', 'underneath', 'were', 'g',
              'actual', 'shall', 'twice', 'rare', "she's", 'himself', 'was', 'three',
              'few', 'forth', 'ah', 'both', 'yet', 'in', 'insofar', 'another',
              'ninthly', 'forgo', 'wherever', 'by', "don't", 'quite', 'seventeenth',
              'third', "couldn't", 'frankly', 'seventhly', 'usually', 'over', "i'd",
              'afterwards', 'never', 'secondly', 'probably', 'way', 'ninety',
              'ninetieth', 'must', 'seventh', 'as', 'several', 'lastly', "they're",
              'nineteenth', 'that', 'will', 'thither', 'sixteenth', "i'll", 'eleventh',
              'everybody', 'soon', 'am', 'is', 'literally', 'couldst', "you'll", 'hers',
              'theirs', "haven't", 'off', 'yesterday', 'there', "you'd", 'undoubtedly',
              'according', 'much', 'next', "you're", 'seven', 'some', 'already', 'q',
              'two', 'wouldst', 'your', 'nine', 'mine', 'nineteen', 'front', 'eighty',
              'i', 'other', 'before', "where's", 'thee', 'rather', 'instead',
              'excepting', 'no', 'unto', 'wherefore', 'her', 'whence', "hasn't",
              'million', 'fourteenth', 'sixty', 'sixtieth', 'possibly', 'ought',
              'downright', 'z', 'why', 'either', "what's", 'thirteenth', 'doubtfully',
              'since', 'eighteenth', 'everywhere', "shan't", 'else', 'ago', 'thirty',
              'seldom', "isn't", 'has', 'what', 'down', 'because', 'against', 'those',
              'which', 'above', 'presumably', 'can', 'fifty', 'when', "he'd", 'or',
              'eight', "wouldn't", "ain't", 'an', 'forever', 'fourthly', 'many',
              'later', 'eleven', 'each', 'getting', 'how', 'would', 'eighteen',
              'whether', 'finally', 'ho', 'outside', 'with', 's', 't', 'oh', 'forty',
              'fifteenth', 'enough', 'for', 'yourself', 'their', 'till', 'now', 'oft',
              'today', 'n', 'onto', 'upon', 'always', "it's", 'despite', 'have',
              'rarely', 'previous', 'shouldst', 'done', 'll', 'whomever', 'x',
              'however', 'whereas', 'might', "weren't", 'below', "they'll", "who's",
              'nevertheless', 'among', 'us', 'previously', 'on', 'if', 'anyone',
              'perhaps', 'tell', 'around', 'four', 'instance', 'really', 'who', 'hence',
              'neither', 'nor', 'except', "wasn't", 'b', 'round', 'likely', 'somebody',
              'h', 'certain', 'erst', 'everyone', 'virtually', 'fifth', 'he',
              'otherwise', 'wil', 'maybe', 'let', 'same', 'said', 'ours', 'soever',
              'about', "who'd", 'c', 'ever', 'thine', "there's", 'seventy', 'been',
              'one', 'nearby', 'near', 'others', 'where', 'whom', "we'll", 'everything',
              'though', 'a', 'through', 'beneath', 'yes', 'inasmuch', 'do',
              'accordingly', "hadn't", 'less', 'beyond', 'into', 'subsequently',
              'wilst', 'thou', 'therefore', 'eighth', 'towards', 'earlier', 'within',
              'after', 'unusually', 'should', 'similarly', "doesn't", 'like', 'this',
              'eightieth', 'these', 'throughout', "we're", 'certainly', 'doth', 'noone',
              'fifteen', "we'd", 'thus', 'nay', 'seventieth', 'sometimes', 'so',
              'anywhere', 'she', 'themselves', 'twenty', 'besides', 'too', 'whenever',
              'dear', 'due', 'twas', 'having', 'thirdly', 'wilt', 'at', 'does', 'you',
              'tenth', 'thirtieth', 'first', 'somewhat', 'sixteen', 'f', "he'll",
              'sixth', 'yours', 'past', 'not', 'tis', 'inside', 'aside', 'nothing',
              'well', 'eventually', 'may', 'ourselves', 'without', "you've", 'unlikely',
              'cannot', 'canst', "let's", 'up', 'more', "i'm", 'hast', 'could', 'say',
              'tomorrow', 'definite', 'generally', 'also', 'gets', 'all', 'extremely',
              'herself', "that's", 'whatever', 'sure', 'almost', 'hundredth', 'exeunt',
              'definitely', 'very', 'whiles', 'twill', 'to', 'occasionally', 'provided',
              'thence', 'yourselves', 'thousand', 'y', "how's", 'somewhere', 'thy',
              'truly', 'j', 'nearly', 'most', 'even', "we've", 'prior', "i've",
              'hardly', 'm', "mustn't", 'along', "aren't", 'although', 'meanwhile',
              'none', 'myself', 'furthermore', 'second', 'eighthly', 'doing',
              'regarding', 'simply', 'my', 'and', "it'd", 'l', 'dost', 'itself',
              'anything', 'than', 'hundred', 'they', 'fiftieth', 'but', 'early',
              'fewer', 'five', 'hath', 'across', 'being', 'our', 'thirteen', 'tenthly',
              'fourteen', 'billionth', 'back', 'undergo', 'any', 'possible',
              'nonetheless', 'ere', 'away', 'be', 'twelfth', 'while', 'only', 'just',
              "won't", 'had', 'shalt', 'u', 'seventeen', 'the', 'fourth', 'o', "can't",
              'behind', 'here', 'then', 'better', 'last', 'toward', 'far', "who'll",
              'such', 'once', 'often', 'unless', 'his', 'afterward', 'until', 'we',
              'every', 'firstly', 'consequently', 'between', 'twentieth',
              'incidentally', 'concerning', 'them', 'of', "they've", 'get', 'nobody',
              'its', "they'd", "she'd", 'whose', 'usual', 'wast', 'someone', 'whoever',
              'k', 'd', 'beside', 'ye', 'order', 'r', 'actually', 'hither', 'something',
              'six', 'under', 'p', 'him', 'me', "she'll", 'e', 'billion', 'fifthly',
              'fortieth', 'art', 'ten', 'twelve', 'did', 'nowhere', 'from', 'likewise'}
# fmt: on

IGNORED_TOKENS = STOP_WORDS | set(punctuation)

# init (de)tokenizer, vocab, and faiss-indexed embeddings
tokenizer = MosesTokenizer(lang="en")
detokenizer = MosesDetokenizer(lang="en")
vocab = []
print("Loading and indexing GloVe-6B (300d) may take several minutes.")
embeddings = np.empty((400000, 300))
with open(glv6b) as f:
    for idx, line in enumerate(f):
        values = line.split()
        vocab.append(values[0])
        embeddings[idx] = np.asarray(values[1:])
f_index = faiss.IndexFlatL2(300)
f_index.add(embeddings)
print("Done loading and indexing.")


def sub_synonym(sentence: str, random_state: Optional[int] = None) -> str:
    """Replace a word in an English string with one of its ten nearest neighbors found
    in GloVe-6B embeddings. Stop words, punctuation, and digits will not be replaced.
    This function assumes that the input is long enough to have at least one content
    word that can be found in GloVe-6B's vocabulary and hence can be replaced;
    otherwise, a runtime error will be raised.

    Args:
        sentence: An English string.
        random_state: A Random seed.

    Returns:
        A string with one word changed to its synonym.

    """
    random.seed(random_state)
    # find a token to replace which not in `IGNORED_TOKENS` but in `vocab`
    tokens = tokenizer.tokenize(sentence)
    tokens_tmp = [
        t
        for t in tokens
        if (
            (t.lower() not in IGNORED_TOKENS)
            and (not t.isdigit())
            and t.lower() in vocab
        )
    ]
    # in case no candidate token to replace
    if len(tokens_tmp):
        token_to_sub = random.choice(tokens_tmp)
    else:
        raise RuntimeError(f"Error: No replaceable token in {sentence=}.")
    token_to_sub_idx = vocab.index(token_to_sub.lower())
    q = embeddings[token_to_sub_idx].reshape(1, -1)

    # sample from 10 nearest neighbors
    _, syn_indices = f_index.search(q, 10 + 1)
    syn = vocab[random.choice(syn_indices.ravel()[1:])]
    position = [t.lower() for t in tokens].index(token_to_sub.lower())
    # preserve capitalization naively
    tokens[position] = syn if token_to_sub[0].islower() else syn.capitalize()

    return detokenizer.detokenize(tokens)


# additional "documentation" in the form of simple tests
if __name__ == "__main__":
    # sentence is from nyt_eng
    sentence = (
        """Jolene Coyle, the owner of Job Place, placed a banner Saturday bearing """
        """a statement against same-sex marriages outside a Granard Street business """
        """building she owns."""
    )
    expected = (
        """Jolene Coyle, the owner of Job Place, placed a banner Saturday bearing """
        """a statement against same-sex marriages outside a Granard Street business """
        """building she purchased."""
    )
    assert sub_synonym(sentence, random_state=17) == expected
    try:
        sub_synonym("This is 123.")
    except RuntimeError as e:
        assert repr(e) == (
            """RuntimeError("Error: No replaceable token in """
            """sentence='This is 123.'.")"""
        )
